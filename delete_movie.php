<?php
error_reporting( error_reporting() & ~E_NOTICE );
// include function files for this application
require_once('movie_sc_fns.php');
session_start();

do_html_header("Deleting movie");
if (check_admin_user()) {
  if (isset($_POST['movieID'])) {
    $movieID = $_POST['movieID'];
    if(delete_movie($movieID)) {
      echo "<p>movie ".$movieID." was deleted.</p>";
    } else {
      echo "<p>movie ".$movieID." could not be deleted.</p>";
    }
  } else {
    echo "<p>We need an movieID to delete a movie.  Please try again.</p>";
  }
  do_html_url("admin.php", "Back to administration menu");
} else {
  echo "<p>You are not authorised to view this page.</p>";
}

do_html_footer();

?>
