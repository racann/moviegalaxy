<?php
error_reporting( error_reporting() & ~E_NOTICE );
// include function files for this application
require_once('movie_sc_fns.php');
session_start();

do_html_header("Add a movie");
if (check_admin_user()) {
  display_movie_form();
  do_html_url("admin.php", "Back to administration menu");
} else {
  echo "<p>You are not actorized to enter the administration area.</p>";
}
do_html_footer();

?>
