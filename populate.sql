USE movie_galaxy;


INSERT INTO movies VALUES ('0000000002','Arnold Schwarzenegger','The Terminator',1,19.99,
'A deadly cyborg tries to change the future.');
INSERT INTO movies VALUES ('0000000003','Van Damme','Bloodsport',1,14.99,
'A martial artits fights in the deadly Kumite for his teachers honor');
INSERT INTO movies VALUES ('0000000001','Sylvester Stallone','Cobra',1,19.99,
'A cop cleans up the streets with violence');

INSERT INTO movies VALUES ('00000000005','Lenardo DiCaprio','The Titanic',4,19.99,
'Love story on the titanic.');

INSERT INTO movies VALUES ('0000000006','Jim Carey','Ace Ventura',2,19.99,
'A cop who oves animals fights crime.');
INSERT INTO movies VALUES ('0000000008','Chris Farley','Tommy Boy',2,14.99,
'An idoit tries to sell brake pads');
INSERT INTO movies VALUES ('0000000007','Adam Sandler','Happy Gilmore',2,19.99,
'A hockey player plays golf');

INSERT INTO movies VALUES ('0000000009','Jamie Lee Curtis','Halloween',3,19.99,
'A deadly killer is on the loose.');

INSERT INTO categories VALUES (1,'Action');
INSERT INTO categories VALUES (2,'Comedy');
INSERT INTO categories VALUES (3,'Horror');
INSERT INTO categories VALUES (4,'Romance');

INSERT INTO admin VALUES ('admin', sha1('admin'));
