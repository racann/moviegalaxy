<?php
error_reporting( error_reporting() & ~E_NOTICE );
// include function files for this application
require_once('movie_sc_fns.php');
session_start();

do_html_header("Adding a movie");
if (check_admin_user()) {
  if (filled_out($_POST)) {
    $movieID = $_POST['movieID'];
    $title = $_POST['title'];
    $actor = $_POST['actor'];
    $catid = $_POST['catid'];
    $price = $_POST['price'];
    $description = $_POST['description'];

    if(insert_movie($movieID, $title, $actor, $catid, $price, $description)) {
      echo "<p>movie <em>".stripslashes($title)."</em> was added to the database.</p>";
    } else {
      echo "<p>movie <em>".stripslashes($title)."</em> could not be added to the database.</p>";
    }
  } else {
    echo "<p>You have not filled out the form.  Please try again.</p>";
  }

  do_html_url("admin.php", "Back to administration menu");
} else {
  echo "<p>You are not authorised to view this page.</p>";
}

do_html_footer();

?>
