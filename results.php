<?php
error_reporting( error_reporting() & ~E_NOTICE );
include ('movie_sc_fns.php');
  // The shopping cart needs sessions, so start one
  session_start();
  //create variables
$searchtype = $_POST['searchtype'];
$searchterm = trim($_POST['searchterm']);

  $movie_array = get_movies_from_search($searchtype, $searchterm);

  do_html_header("Search Results");

  // get the movie info out from db

  display_movies($movie_array);


  // if logged in as admin, show add, delete movie links
  if(isset($_SESSION['admin_user'])) {
    display_button("index.php", "continue", "Continue Shopping");
    display_button("admin.php", "admin-menu", "Admin Menu");
    display_button("edit_category_form.php?catid=".$catid,
                   "edit-category", "Edit Category");
  } else {
    display_button("index.php", "continue-shopping", "Continue Shopping");
  }

  do_html_footer();
?>
