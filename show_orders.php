<?php
error_reporting( error_reporting() & ~E_NOTICE );
include ('movie_sc_fns.php');
  // The shopping cart needs sessions, so start one
  session_start();


  do_html_header("Orders");

  show_past_orders();


  // if logged in as admin, show add, delete movie links
  if(isset($_SESSION['admin_user'])) {
    display_button("index.php", "continue", "Continue Shopping");
    display_button("admin.php", "admin-menu", "Admin Menu");
  } else {
    display_button("index.php", "continue-shopping", "Continue Shopping");
  }

  do_html_footer();
?>