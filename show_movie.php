<?php
error_reporting( error_reporting() & ~E_NOTICE );
  include ('movie_sc_fns.php');
  // The shopping cart needs sessions, so start one
  session_start();

  $movieId = $_GET['movieId'];

  // get this movie out of database
  $movie = get_movie_details($movieId);
  do_html_header($movie['title']);
  display_movie_details($movie);

  // set url for "continue button"
  $target = "index.php";
  if($movie['catid']) {
    $target = "show_cat.php?catid=".$movie['catid'];
  }

  // if logged in as admin, show edit movie links
  if(check_admin_user()) {
    display_button("edit_movie_form.php?movieId=".$movieId, "edit-item", "Edit Item");
    display_button("admin.php", "admin-menu", "Admin Menu");
    display_button($target, "continue", "Continue");
  } else {
    display_button("show_cart.php?new=".$movieId, "add-to-cart",
                   "Add".$movie['title']." To My Shopping Cart");
    display_button($target, "continue-shopping", "Continue Shopping");
  }

  do_html_footer();
?>
